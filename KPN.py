# Przed całą logiką gry potrzebujemy zaimportować RANDINT
# by móc losowo wygenerować cyfrę 0-2.
from random import randint
print("Kamień...")
print("Papier...")
print("Nożyce...")
#Przechwytujemy wartość wpisaną przez gracza
#  .lower() służy do konwertowania wartości do
# lowercase, aby gra nie rozrozniala wielkości znaków
# Na przykład KaMien -> kamien
wybor1 = input("Dokonaj wyboru: ").lower()
# losujemy cyfrę z przedziału 0-2 i przypisujemy jej wartość
komputer = randint(0,2)
if komputer == 0:
	komputer = "kamien"
elif komputer == 1:
	komputer = "papier"
else:
	komputer = "nozyce"
# Pokazujemy, co wylosował komputer
print(f"Komputer zagrał {komputer}")
# Jezeli wartość gracza = wartości wylosowanej przez komputer
# mamy remis
if wybor1 == komputer:
	print("Remis!")
# Zasady gry w kamień papier nożyce:
# or wybor 1 == "kamień" to obsługa polskich znaków

if wybor1 == "kamien" or wybor1 == "kamień":
	if komputer =="nozyce":
		print("Wygrałeś!")
	elif komputer =="papier":
		print("Wygrał komputer")

elif wybor1 == "nozyce" or wybor1 == "nożyce":
	if komputer == "kamien":
		print("Wygrał komputer!")
	elif komputer == "papier":
		print("Wygrałeś!")

elif wybor1 == "papier":
	if komputer == "kamien":
		print("Wygrałeś!")
	elif komputer == "nozyce":
		print("Wygral komputer")
else:
	print("Gra nierozegrana, nie rozumiem polecenia!")